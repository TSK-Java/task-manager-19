package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.model.Task;

import java.util.*;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (description == null || description.isEmpty())
            throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Task create(final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Task task = create(name, description);
        if (task == null) throw new TaskNotFoundException();
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty())
            throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        final Task task = findOneById(id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= repository.count())
            throw new IndexOutOfBoundsException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        final Task task = findOneByIndex(index);
        if (task == null)
            throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        final Task task = findOneById(id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setStatus(status);

        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= repository.count())
            throw new IndexOutOfBoundsException();
        if (status == null)
            throw new StatusEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null)
            throw new TaskNotFoundException();
        task.setStatus(status);

        return task;
    }

}
