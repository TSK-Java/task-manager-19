package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.model.Project;

import java.util.Date;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (description == null || description.isEmpty())
            throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        final Project project = findOneById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= repository.count())
            throw new IndexOutOfBoundsException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();

        final Project project = findOneByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        final Project project = findOneById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);

        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= repository.count())
            throw new IndexOutOfBoundsException();
        if (status == null)
            throw new StatusEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);

        return project;
    }

}
