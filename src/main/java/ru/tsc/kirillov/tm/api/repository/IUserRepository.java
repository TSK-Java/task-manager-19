package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

}
