package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);


}
