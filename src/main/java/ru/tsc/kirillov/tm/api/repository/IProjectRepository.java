package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
