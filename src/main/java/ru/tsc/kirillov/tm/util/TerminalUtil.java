package ru.tsc.kirillov.tm.util;

import ru.tsc.kirillov.tm.exception.system.value.ValueNotValidInteger;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        String valueStr = null;
        try {
            valueStr = nextLine();
            return Integer.parseInt(valueStr);
        }
        catch (final NumberFormatException e) {
            ValueNotValidInteger childException = new ValueNotValidInteger(valueStr);
            childException.initCause(e);
            throw childException;
        }
    }

    static Date nextDate() {
        return DateUtil.toDate(nextLine());
    }

}
