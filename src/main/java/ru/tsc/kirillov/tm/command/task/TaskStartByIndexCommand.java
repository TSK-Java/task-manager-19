package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Запустить задачу по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Запуск задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        getTaskService().changeTaskStatusByIndex(NumberUtil.fixIndex(index), Status.IN_PROGRESS);
    }

}
