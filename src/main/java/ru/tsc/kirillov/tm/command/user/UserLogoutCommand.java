package ru.tsc.kirillov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "Выход из пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Выход]");
        getAuthService().logout();
    }

}
