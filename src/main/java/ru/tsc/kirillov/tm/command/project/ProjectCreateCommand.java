package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Создание нового проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Создание проекта]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        final Date dateEnd = TerminalUtil.nextDate();
        getProjectService().create(name, description, dateBegin, dateEnd);
    }

}
