package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Удалить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Удаление проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        getProjectTaskService().removeProjectById(project.getId());
    }

}
