package ru.tsc.kirillov.tm.command;

import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public String getArgument() {
        return null;
    }

    public abstract String getDescription();

    public abstract void execute();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();

        if (name != null && !name.isEmpty())
            result += name;
        if (argument != null && !argument.isEmpty())
            result += ", " + argument;
        if (description != null && !description.isEmpty())
            result += " - " + description;

        if (!result.isEmpty())
            return result;
        else
            return super.toString();
    }

}
