package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.exception.field.UserNotFoundException;
import ru.tsc.kirillov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "view-profile";
    }

    @Override
    public String getDescription() {
        return "Отображение профиля пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение профиля пользователя]");
        final User user = getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("Логин: " + user.getLogin());
        System.out.println("Имя: " + user.getFirstName());
        System.out.println("Фамилия: " + user.getLastName());
        System.out.println("Отчество: " + user.getMiddleName());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Роль: " + user.getRole());
    }

}
