package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Удалить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Удаление задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(id);
    }

}
